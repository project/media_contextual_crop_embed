<?php

declare(strict_types=1);

namespace Drupal\media_contextual_crop_embed\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\media_contextual_crop\MediaContextualCropPluginManager;
use Drupal\media_contextual_crop_embed\MediaContextualCropEmbedService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Media Embed contextual Cropping form.
 */
class ModalForm extends FormBase {

  /**
   * Embed service.
   *
   * @var \Drupal\media_contextual_crop_embed\MediaContextualCropEmbedService
   */
  protected $embedService;

  /**
   * Media Contextual Plugins Manager.
   *
   * @var \Drupal\media_contextual_crop\MediaContextualCropPluginManager
   */
  protected $mediaContextualCropPluginManager;

  /**
   * Class construct.
   *
   * @param \Drupal\media_contextual_crop_embed\MediaContextualCropEmbedService $embedService
   *   Embed service.
   * @param \Drupal\media_contextual_crop\MediaContextualCropPluginManager $mediaContextualCropPluginManager
   *   Media Contextual Plugins Manager.
   */
  public function __construct(MediaContextualCropEmbedService $embedService, MediaContextualCropPluginManager $mediaContextualCropPluginManager) {
    $this->embedService = $embedService;
    $this->mediaContextualCropPluginManager = $mediaContextualCropPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('media_contextual_crop_embed.service'),
      $container->get('plugin.manager.media_contextual_crop')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_contextual_crop_embed_modal';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $build_info = $form_state->getBuildInfo();

    $form['#prefix'] = '<div id="editor-crop-dialog-form">';
    $form['#suffix'] = '</div>';

    // Save build info in hidden for rebuild during submit.
    if (isset($build_info['args'][0]['editor_instance_id']) && $build_info['args'][0]['editor_instance_id'] != '') {
      foreach ($build_info['args'][0] as $param => $value) {
        $form[$param] = [
          '#type' => 'hidden',
          '#value' => $value,
        ];
      }
    }

    // The Crop widget are dictated by media VIEW mode (not form).
    $this->embedService->addCropWidgetForCk5($form, $form_state, $build_info['args'][0] ?? []);

    if (!isset($form['no_crop_message'])) {
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save crop'),
        '#attributes' => [
          'class' => [
            'use-ajax',
            'ui-widget',
          ],
        ],
        '#submit' => [],
        '#ajax' => [
          'callback' => '::submitForm',
          'event' => 'click',
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $build_info = $form_state->getBuildInfo();
    $media_uuid = $build_info['args'][0]['media_uuid'];

    $plugin_id = $form_state->getValue('cropType');
    $plugin = $this->mediaContextualCropPluginManager->createInstance($plugin_id);
    $plugin->widgetSave($form, $form_state);

    $values = [
      'media_uuid' => $media_uuid,
      'crop' => $form_state->getValue('attributes'),
    ];

    $response = new AjaxResponse();
    $response->addCommand(new EditorDialogSave($values));
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

}
