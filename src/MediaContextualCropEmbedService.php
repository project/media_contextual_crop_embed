<?php

namespace Drupal\media_contextual_crop_embed;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\media\MediaInterface;
use Drupal\media_contextual_crop\MediaContextualCropPluginManager;
use Drupal\media_contextual_crop\MediaContextualCropService;

/**
 * Service description.
 */
class MediaContextualCropEmbedService {

  use StringTranslationTrait;

  /**
   * The plugin.manager.media_contextual_crop service.
   *
   * @var \Drupal\media_contextual_crop\MediaContextualCropPluginManager
   */
  protected $mediaContextualCropPluginManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Media Contextual Crop Service.
   *
   * @var \Drupal\media_contextual_crop\MediaContextualCropService
   */
  protected $mccService;

  /**
   * The Get EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a MultiCropEmbedService object.
   *
   * @param \Drupal\media_contextual_crop\MediaContextualCropPluginManager $media_contextual_crop
   *   The plugin.manager.media_contextual_crop service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\media_contextual_crop\MediaContextualCropService $mcc_service
   *   Media Contextual Crop Service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Get entity_type_manager.
   */
  public function __construct(MediaContextualCropPluginManager $media_contextual_crop,
                              EntityDisplayRepositoryInterface $entity_display_repository,
                              EntityRepositoryInterface $entity_repository,
                              MediaContextualCropService $mcc_service,
                              EntityTypeManagerInterface $entity_type_manager) {
    $this->mediaContextualCropPluginManager = $media_contextual_crop;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityRepository = $entity_repository;
    $this->mccService = $mcc_service;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Gets the name of an image media item's source field.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media item being embedded.
   *
   * @return string|null
   *   The name of the image source field configured for the media item, or
   *   NULL if the source field is not an image field.
   */
  private function recoverSourceField(MediaInterface $media): ?string {
    $field_definition = $media->getSource()
      ->getSourceFieldDefinition($media->bundle->entity);
    $item_class = $field_definition->getItemDefinition()->getClass();
    if (is_a($item_class, ImageItem::class, TRUE)) {
      return $field_definition->getName();
    }
    return NULL;
  }

  /**
   * Find crops needed in media view mode.
   *
   * @param array $settings
   *   Formatter settings.
   * @param string $media_bundle
   *   Media bundle.
   * @param string $source_field
   *   Media source field.
   *
   * @return array|null
   *   List of crop plugin & settings.
   */
  public function loadCropPlugins(array $settings, $media_bundle, $source_field) {

    $cropPlugins = [];
    $view_mode = $settings['settings']['default_view_mode'] ?? 'default';

    // Load view mode of bundle.
    $media_display = $this->entityDisplayRepository->getViewDisplay('media', $media_bundle, $view_mode);
    $field_settings = $media_display->getComponent($source_field);

    // If view mode has an image style.
    if (isset($field_settings['settings']['image_style']) &&
      ($field_settings['type'] == 'contextual_image' || $field_settings['type'] == 'image'))
    {
      $image_style_id = $field_settings['settings']['image_style'];
      $plugin = $this->getCompatiblePlugin($image_style_id);
      if ($plugin != NULL) {
        $cropPlugins[$image_style_id] = $plugin;
      }
    }

    // If view mode is a responsive Image.
    if (isset($field_settings['settings']['responsive_image_style']) && $field_settings['type'] == 'responsive_image') {

      // Get Responsive config data.
      $responsive_image_style = $this->entityTypeManager->getStorage('responsive_image_style')->load($field_settings['settings']['responsive_image_style']);

      if ($responsive_image_style != NULL) {
        // Check if fallback is defined and use crop.
        $fallback = $responsive_image_style->getFallbackImageStyle();
        if ($this->mccService->styleUseMultiCrop($fallback)) {
          $plugin = $this->getCompatiblePlugin($fallback);
          if ($plugin != NULL) {
            $cropPlugins[$fallback] = $plugin;
          }
        }

        // Parse each style, find someone...
        // Find styles.
        $style_mapping = $responsive_image_style->getImageStyleMappings();
        foreach ($style_mapping as $data) {
          foreach ($data['image_mapping']['sizes_image_styles'] as $style) {
            if ($this->mccService->styleUseMultiCrop($style)) {
              $plugin = $this->getCompatiblePlugin($style);
              if ($plugin != NULL) {
                $cropPlugins[$style] = $plugin;
              }
            }
          }

        }
      }

    }

    return $cropPlugins;
  }

  /**
   * Find the best crop adapter for this use.
   *
   * @param string $style
   *   Image style name.
   *
   * @return array|mixed
   *   Crop adapter information.
   */
  private function getCompatiblePlugin($style) {

    static $checked = [];

    if (!isset($checked[$style])) {

      $cropPlugins = [];
      /** @var \Drupal\image\Entity\ImageStyle $image_style */
      $image_style = $this->entityRepository->loadEntityByConfigTarget('image_style', $style);
      if (isset($image_style)) {
        $effects = $image_style->getEffects()->getConfiguration();
        $plugins = $this->mediaContextualCropPluginManager->getDefinitions();

        // Find crops used in image style.
        foreach ($effects as $effect) {
          foreach ($plugins as $plugin) {
            if (in_array($effect['id'], $plugin['image_style_effect'])) {
              if (!isset($cropPlugins[$plugin['id']])) {
                $cropPlugins[$plugin['id']] = [$effect['data']['crop_type']];
              }
              else {
                $cropPlugins[$plugin['id']][] = $effect['data']['crop_type'];
              }
            }
          }
        }
        $checked[$style] = $cropPlugins;
      }
      else {
        // If not a style...
        $checked[$style] = NULL;
      }
    }
    return $checked[$style];
  }

  /**
   * Add Crop widget to a form.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   FormState.
   * @param array $args
   *   FormStates Build args.
   */
  public function addCropWidgetForCk5(array &$form, FormStateInterface $form_state, array $args) {

    /** @var \Drupal\media\MediaInterface $media */
    $media = $this->entityRepository->loadEntityByUuid('media', $args['media_uuid']);
    $source_field_name = $this->recoverSourceField($media);

    $plugin_settings = [];

    // Check if data exist in build Info.
    $data_source = $args;
    if (isset($data_source['editor_instance_id']) && $data_source['editor_instance_id'] == '') {
      $data_source = $form_state->getUserInput();
    }

    if ($data_source['selectedViewMode'] != '') {
      $view_mode = $data_source['selectedViewMode'];
    }
    else {
      $view_mode = $data_source['default_view_mode'];
    }

    $plugin_settings['settings']['default_view_mode'] = $view_mode;
    $plugins = $this->loadCropPlugins($plugin_settings, $media->bundle(), $source_field_name);

    if (count($plugins) == 0) {
      // No Crop found for this view mode.
      $form['no_crop_message'] = [
        '#markup' => $this->t('No crop defined for the view mode @view_mode. Please close the modal for changing view mode.', ['@view_mode' => $view_mode]) . '<br />',
      ];
    }

    $structured_plugin = [];
    foreach ($plugins as $style => $plugin_data) {
      foreach ($plugin_data as $plugin_id => $image_style_crops) {
        if (!isset($structured_plugin[$plugin_id])) {
          $structured_plugin[$plugin_id] = [];
        }
        if (!isset($structured_plugin[$plugin_id][$style])) {
          $structured_plugin[$plugin_id][$style] = $image_style_crops;
        }
      }
    }

    // Load multi crop plugins.
    foreach ($structured_plugin as $plugin_id => $image_style_crops) {
      $crops = [];
      foreach ($image_style_crops as $styles) {
        $crops[$styles[0]] = $styles[0];
      }

      /** @var \Drupal\media_contextual_crop\MediaContextualCropInterface $plugin */
      $plugin = $this->mediaContextualCropPluginManager->createInstance($plugin_id);

      $media_library_form_display = $this->entityDisplayRepository->getFormDisplay('media', $media->bundle());

      $media_embed_element = [];

      // If the crop already contextual, use it.
      if ($data_source['oldCrop'] != '' && $data_source['oldCropType'] != '' && $data_source['oldCropType'] == $plugin_id) {
        $media_embed_element =
          [
            'data-crop-settings' => $data_source['oldCrop'],
          ];
      }

      $preview_image_style = $data_source['preview_image_style'] ?? 'thumbnail';

      $component_data = $plugin->getComponentConfig($media_embed_element, $crops, $preview_image_style);

      $media_library_form_display->setComponent($source_field_name, $component_data);

      // Render form.
      $widget = $media_library_form_display->getRenderer($source_field_name);

      $items = $media->get($source_field_name);
      $items->filterEmptyItems();

      // Extract widget of source field.
      $form['#parents'] = [];
      $form[$source_field_name] = $widget->form($items, $form, $form_state);

      // Finishing form.
      $plugin->finishElement($form, $source_field_name, $media_embed_element);
      $form['cropType'] = [
        "#type" => 'hidden',
        "#value" => $plugin->getPluginId(),
      ];
    }
  }

}
