<?php

declare(strict_types=1);

namespace Drupal\media_contextual_crop_embed\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber.
 */
class MultiCropEmbedRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($collection->all() as $route) {
      // Add media_contextual_crop_embed as autorized filter.
      if ($route->getPath() == '/media/{filter_format}/preview') {
        $route->setRequirement('_custom_access', '\Drupal\media_contextual_crop_embed\Controller\AlterMediaFilterController::formatUsesMediaEmbedFilter');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();

    // Use a lower priority than \Drupal\views\EventSubscriber\RouteSubscriber
    // to ensure the requirement will be added to its routes.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -300];

    return $events;
  }

}
