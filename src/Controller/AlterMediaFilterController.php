<?php

declare(strict_types=1);

namespace Drupal\media_contextual_crop_embed\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\filter\FilterFormatInterface;
use Drupal\media\Controller\MediaFilterController;

/**
 * Controller which renders a preview of the provided text.
 *
 * @internal
 *   This is an internal part of the media system in Drupal core and may be
 *   subject to change in minor releases. This class should not be
 *   instantiated or extended by external code.
 */
class AlterMediaFilterController extends MediaFilterController {

  /**
   * Checks access based on media_embed filter status on the text format.
   *
   * @param \Drupal\filter\FilterFormatInterface $filter_format
   *   The text format for which to check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public static function formatUsesMediaEmbedFilter(FilterFormatInterface $filter_format) {
    $filters = $filter_format->filters();
    $media_embed = $filters->has('media_embed') && $filters->get('media_embed')->status;
    $multi_crop = $filters->has('media_contextual_crop_embed') && $filters->get('media_contextual_crop_embed')->status;
    return AccessResult::allowedIf($media_embed || $multi_crop)->addCacheableDependency($filter_format);
  }

}
