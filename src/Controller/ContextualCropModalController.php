<?php

declare(strict_types=1);

namespace Drupal\media_contextual_crop_embed\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Media Embed contextual Cropping routes.
 */
class ContextualCropModalController extends ControllerBase {

  /**
   * Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Class construct.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request Stack.
   */
  public function __construct(RequestStack $requestStack) {
    $this->request = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Builds the response.
   */
  public function build(string $mediaUUID) {

    $dialogOptions = $this->request->request->all('dialogOptions');

    $params = [
      'default_view_mode' => $dialogOptions['defaultViewMode'] ?? 'default',
      'selectedViewMode' => $dialogOptions['selectedViewMode'] ?? '',
      'editor_instance_id' => $dialogOptions['editorInstanceId'] ?? '',
      'media_uuid'   => $mediaUUID,
      'oldCrop' => $dialogOptions['oldCrop'] ?? '',
      'oldCropType' => $dialogOptions['oldCropType'] ?? '',
      'preview_image_style' => $dialogOptions['preview_image_style'] ?? 'thumbnail',
    ];

    $modal_form = $this->formBuilder()->getForm('Drupal\media_contextual_crop_embed\Form\ModalForm', $params);

    $options = [
      'width' => '75%',
    ];

    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand('Contextual Crop', $modal_form, $options));
    return $response;
  }

}
