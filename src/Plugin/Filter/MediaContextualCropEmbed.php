<?php

declare(strict_types=1);

namespace Drupal\media_contextual_crop_embed\Plugin\Filter;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\media\MediaInterface;
use Drupal\media\Plugin\Filter\MediaEmbed;
use Drupal\media_contextual_crop_embed\MediaContextualCropEmbedService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to embed media items using a custom tag.
 *
 * @Filter(
 *   id = "media_contextual_crop_embed",
 *   title = @Translation("Contextual Crop on Embed media"),
 *   description = @Translation("Add contextual Crop capabilities to Embed Media. Order it BEFORE the 'Embed Media' filter."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   settings = {
 *     "default_view_mode" = "default",
 *     "allowed_view_modes" = {},
 *     "allowed_media_types" = {},
 *   },
 *   weight = 98,
 * )
 *
 * @internal
 */
class MediaContextualCropEmbed extends MediaEmbed implements ContainerFactoryPluginInterface, TrustedCallbackInterface {
  /**
   * The current route service.
   *
   * @var \Drupal\media_contextual_crop_embed\MediaContextualCropEmbedService
   */
  protected MediaContextualCropEmbedService $mediaContextualCropEmbedService;

  /**
   * Constructs a MultiCropEmbed object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\media_contextual_crop_embed\MediaContextualCropEmbedService $media_contextual_crop_embed_service
   *   The media_contextual_crop_embed service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityRepositoryInterface $entity_repository, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, EntityTypeBundleInfoInterface $bundle_info, RendererInterface $renderer, LoggerChannelFactoryInterface $logger_factory, MediaContextualCropEmbedService $media_contextual_crop_embed_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_repository, $entity_type_manager, $entity_display_repository, $bundle_info, $renderer, $logger_factory);
    $this->mediaContextualCropEmbedService = $media_contextual_crop_embed_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.repository'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('renderer'),
      $container->get('logger.factory'),
      $container->get('media_contextual_crop_embed.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function applyPerEmbedMediaOverrides(\DOMElement $node, MediaInterface $media) {

    parent::applyPerEmbedMediaOverrides($node, $media);
    if ($image_field = $this->getMediaImageSourceField($media)) {
      if ($node->hasAttribute('data-crop-settings')) {
        $media->{$image_field}->media_contextual_crop_embed_settings = $node->getAttribute('data-crop-settings');
        $media->{$image_field}->media_contextual_crop_plugin_type = $node->getAttribute('data-crop-type');
        $media->{$image_field}->media_contextual_crop_context = $node->getAttribute('data-crop-context');
        $node->removeAttribute('data-crop-settings');
      }
    }
  }

}
