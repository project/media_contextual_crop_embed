<?php

declare(strict_types=1);

namespace Drupal\media_contextual_crop_embed\Plugin\MediaContextualCropUseCase;

use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\media_contextual_crop\MediaContextualCropUseCasePluginBase;

/**
 * Plugin implementation of the media_contextual_crop_use_case.
 *
 * @MediaContextualCropUseCase(
 *   id = "embed",
 *   label = @Translation("Embed"),
 *   default_folder = "multi_crop_embed"
 * )
 */
class Embed extends MediaContextualCropUseCasePluginBase {

  /**
   * {@inheritdoc}
   */
  public function isCompetent(ImageItem $item) {
    return (isset($item->media_contextual_crop_plugin_type) && isset($item->media_contextual_crop_embed_settings));
  }

  /**
   * {@inheritdoc}
   */
  public function getCropSettings(ImageItem $item) {
    if ($plugin_id = $item->media_contextual_crop_plugin_type) {

      $embed_data = [];
      if ($embed_data['crop'] = $item->media_contextual_crop_embed_settings) {

        // Get context.
        $embed_data['context'] = $item->media_contextual_crop_context;

        // Load contextual Crop plugins.
        /** @var \Drupal\media_contextual_crop\MediaContextualCropInterface $plugin */
        $plugin = $this->mccPluginManager->createInstance($plugin_id);

        // Get usable CROP settings.
        $media_contextual_crop_settings = $plugin->processEmbedData($embed_data);

        return $media_contextual_crop_settings;
      }
    }

    return NULL;
  }

}
