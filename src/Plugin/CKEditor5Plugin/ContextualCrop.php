<?php

declare(strict_types=1);

namespace Drupal\media_contextual_crop_embed\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\ckeditor5\Plugin\CKEditor5PluginElementsSubsetInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\EditorInterface;
use Drupal\media_contextual_crop\MediaContextualCropService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CKEditor 5 Advanced Link plugin.
 */
class ContextualCrop extends CKEditor5PluginDefault implements ContainerFactoryPluginInterface, CKEditor5PluginConfigurableInterface, CKEditor5PluginElementsSubsetInterface {

  use CKEditor5PluginConfigurableTrait;

  /**
   * The MCC service.
   *
   * @var \Drupal\media_contextual_crop\MediaContextualCropService
   */
  protected $mediaContextualCropService;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a MultiCropEmbed object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity Type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\media_contextual_crop\MediaContextualCropService $media_contextual_crop_service
   *   The media_contextual_crop service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, MediaContextualCropService $media_contextual_crop_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mediaContextualCropService = $media_contextual_crop_service;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('media_contextual_crop.service')
    );
  }

  /**
 * The default configuration for this plugin.
 *
 * @var string[][]
 */
  const DEFAULT_CONFIGURATION = [
    'preview_image_style' => 'crop_thumbnail',
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return static::DEFAULT_CONFIGURATION;
  }

  /**
   * Gets all enabled attributes.
   *
   * @return string[]
   *   The values in the plugins.ckeditor5_link.enabled_attributes config.
   */
  private function getEnabledAttributes(): array {
    return ['preview_image_style' => $this->configuration['preview_image_style']];
  }

  /**
   * {@inheritdoc}
   *
   * Form for choosing which additional <a> attributes are available.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $image_style_options = [];
    $image_styles = $this->entityTypeManager->getStorage('image_style')->loadMultiple();
    foreach ($image_styles as $key => $image_style) {
      if (!$this->mediaContextualCropService->styleUseMultiCrop($key)) {
        $image_style_options[$key] = $image_style->label();
      }
    }

    $form['preview_image_style'] = [
      '#type' => 'select',
      '#options' => $image_style_options,
      '#title' => $this->t('Image Style used in Crop modal'),
      '#default_value' => $this->configuration['preview_image_style'],
      '#description' => $this->t('Only image style which not using crop are available. Do not choose an image style that alters the aspect ratio of the original image.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $preview_image_style = $form_state->getValue('preview_image_style');
    if ($this->mediaContextualCropService->styleUseMultiCrop($preview_image_style)) {
      $form_state->setErrorByName(
        'preview_image_style',
        $this->t('Image Style used in Crop modal : Do not choose an image style that uses a crop.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['preview_image_style'] = $form_state->getValue('preview_image_style');
  }

  /**
   * {@inheritdoc}
   *
   * Filters the enabled attributes to those chosen in editor config.
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {

    $media_embed_filter = $editor->getFilterFormat()->filters('media_embed') ?? NULL;
    $default_view_mode = NULL;
    if ($media_embed_filter != NULL) {
      $settings = $media_embed_filter->getConfiguration();
      $default_view_mode = $settings['settings']['default_view_mode'];
    }

    $data = $this->getEnabledAttributes();
    $data['default_media_view_mode'] = $default_view_mode;

    return [
      'MCCE' => $data,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getElementsSubset(): array {
    $subset[] = '<drupal-media>';
    $subset[] = '<drupal-media data-crop-settings data-crop-type data-crop-context>';
    return $subset;
  }

}
