CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* How use it
* FAQ
* Maintainers

INTRODUCTION
------------

"Media Contextual Crop" collection provide a solution to localy overide crop
setting for media.

This module provide a Filter & CKedidor5 plugin in order to add contextual
croping on native "Media Embed".

This module provide a Contextual Crop "use-case", but need a Contextual Crop
Plugin in ordre to show a crop widget.

REQUIREMENTS
------------

* [Media Contextual Cropping API](https://www.drupal.org/project/media_contextual_crop)
for the plugin definition
* Drupal Core Media Library

RECOMMENDED MODULES
------------

You need a Crop widget for Contextualisation

In the Interfaces you will found :

* [Media Contextual Cropping Focal Point Adapter](https://www.drupal.org/project/media_contextual_crop_fp_adapter)
which provides a plugin to interface for Focal Point Crop module
* [Media Contextual Cropping Image Widget Crop Adapter](https://www.drupal.org/project/media_contextual_crop_iwc_adapter)
which provides a plugin to interface for Image Widget Crop module

INSTALLATION
------------

Install this module as you would normally install a contributed
Drupal module. Visit <https://www.drupal.org/node/1897420> for further
information.

CONFIGURATION
-------------

* Activate the module
* Configure the media image view mode
Go to the view mode, and on the image field, choose the image style configured
with a crop effect (see readme of interfacing module choosed)
* Edit a Text format (admin > configuration > Content authoring > Text
formats and editors > configure)
  * In the toolbar configuration, add "insert form Media Library" plugin
![](https://www.drupal.org/files/toolbar_5.png)
  * In filters, activate "Contextual Crop on Embed media" (not the
nativ "Embed media")
![](https://www.drupal.org/files/Filters.png)
  * In filters Settings
    * In Limit allowed HTML tags :
**Add "data-crop-settings" and "data-crop-type" attributs to the "drupal-media"
HTML tag**
  * configure Embed Media with contextual with :
    * as "Default view mode", the view mode which use the crop plugin
    * as Media types selectable : at least the media type with the crop
  * Save Text format Configuration

[Reference](https://www.drupal.org/docs/contributed-modules/how-use-media-multicroping-11x-branch/preparation-configuration#s-contextual-cropping-for-media-embed-in-wysiwyg)

HOW USE IT
----------

Open a content with a wysiwyg using the text format configured before.

Add a media with the media plugin.

Edit media embed, "et voilà", you can use the crop widget to make a contextual
crop on your media in the wysiwyg

![](https://www.drupal.org/files/dialog_2.png)

FAQ
----

Q: I have the crop widget, but the crop seem not applied on the image

A: On your text format, please check in the "Limit allowed HTML tags" settings
you correctly add "data-crop-settings" and "data-crop-type" attributs to the
"drupal-media" HTML tag

MAINTAINERS
-----------

* Damien ROBERT - <https://www.drupal.org/u/drdam>

Supporting organization:

* SMILE - <https://www.drupal.org/smile>
