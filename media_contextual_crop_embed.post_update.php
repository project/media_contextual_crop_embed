<?php

/**
 * @file
 * Post update files.
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * Add Context to all media embeded.
 */
function media_contextual_crop_embed_post_update_add_context_to_ckeditor(&$sandbox) {

  // Init batch.
  if (!isset($sandbox['init'])) {
    $data = [];
    // Get all field types.
    $field_types = \Drupal::entityTypeManager()
      ->getStorage('field_storage_config')
      ->loadMultiple();
    $types = ['text_long', 'text_with_summary', 'text'];

    foreach ($field_types as $field_storage) {
      // If field type can have a wysiwyg.
      if (!(in_array($field_storage->getType(), $types))) {
        continue;
      }

      // Extract data.
      $entity_type = $field_storage->getTargetEntityTypeId();
      $field_name = $field_storage->get('field_name');

      foreach ($field_storage->getBundles() as $bundle_key => $bundle_name) {
        $data[$entity_type][$bundle_key][] = $field_name;
      }
    }

    // Structure data for batching.
    foreach ($data as $entity_type => $item) {
      $type = \Drupal::entityTypeManager()->getDefinition($entity_type);
      $bundleType = $type->getKey('bundle');
      foreach ($item as $bundle => $fields) {
        $sandbox['init'][] = [
          'type' => $entity_type,
          'bundle_key' => $bundleType,
          'bundle' => $bundle,
          'fields' => $fields,
        ];
      }
    }

    // Init some variables.
    $sandbox['current_key'] = 0;
    $sandbox['current_entity_key'] = 0;
    $sandbox['progress'] = 0;
    $sandbox['nb_entity_by_row'] = 10;

  }

  // Load current segment.
  $current_segment = $sandbox['init'][$sandbox['current_key']];

  // Count present entities for the segment.
  $query_count = \Drupal::entityQuery($current_segment['type']);
  $query_count->accessCheck(FALSE);
  $query_count->condition($current_segment['bundle_key'], $current_segment['bundle']);
  $count = $query_count->count()->execute();

  // If no entities, go next segment.
  if ($count == 0) {
    \Drupal::logger('media_contextual_crop_embed Add context to media_embed')->warning('No entities found for bundle ' . $current_segment['bundle'] . ' of ' . $current_segment['type']);
    $sandbox['progress'] = 0;
    $sandbox['current_key']++;
    $sandbox['#finished'] = ($sandbox['current_key'] + 1) / count($sandbox['init']);
  }
  else {
    // Load a group of entities.
    $query = \Drupal::entityQuery($current_segment['type']);
    $query->accessCheck(FALSE);
    $query->condition($current_segment['bundle_key'], $current_segment['bundle']);
    $query->range($sandbox['progress'], $sandbox['nb_entity_by_row']);
    $entities_id = $query->execute();

    // If we have some ids.
    if (count($entities_id) > 0) {
      // Load entities.
      $entities = \Drupal::entityTypeManager()
        ->getStorage($current_segment['type'])->loadMultiple($entities_id);

      // Check entities for updates.
      foreach ($entities as $entity) {
        media_contextual_crop_embed_update_entity($entity, $current_segment['fields']);
      }
    }

    // If we have load a full stack of entities.
    if (count($entities_id) == $sandbox['nb_entity_by_row']) {
      // It can be have more entities, so just push progess of the entity.
      $sandbox['progress'] += $sandbox['nb_entity_by_row'];
      \Drupal::logger('media_contextual_crop_embed Add context to media_embed')->warning('Updated ' . $sandbox['progress'] . '/' . $count . ' items for bundle ' . $current_segment['bundle'] . ' of ' . $current_segment['type']);
      $sandbox['#finished'] = ($sandbox['current_key'] + 1) / count($sandbox['init']);
    }
    else {
      // We haven't enought entity to a full stack, the entity is finished.
      \Drupal::logger('media_contextual_crop_embed Add context to media_embed')->warning('Fully updated entities (' . $count . ') for bundle ' . $current_segment['bundle'] . ' of ' . $current_segment['type']);
      $sandbox['progress'] = 0;
      $sandbox['current_key']++;
      $sandbox['#finished'] = ($sandbox['current_key'] + 1) / count($sandbox['init']);
    }
  }

}

/**
 * Update a single entity.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   Entity host.
 * @param string[] $field_names
 *   List of field names.
 */
function media_contextual_crop_embed_update_entity(EntityInterface $entity, $field_names) {
  $entity_have_changed = FALSE;

  foreach ($field_names as $field_name) {
    $values = $entity->get($field_name)->getValue();
    $field_have_changed = FALSE;
    foreach ($values as $delta => $value) {
      $new_value = media_contextual_crop_embed_update_entity_process_value($value['value'], $entity, $field_name, $delta);
      if ($new_value !== $value['value']) {
        $field_have_changed = TRUE;
        $values[$delta]['value'] = $new_value;
      }

      if (isset($value['summary'])) {
        $new_value = media_contextual_crop_embed_update_entity_process_value($value['summary'], $entity, $field_name, $delta);
        if ($new_value !== $value['summary']) {
          $field_have_changed = TRUE;
          $values[$delta]['summary'] = $new_value;
        }
      }
    }

    if ($field_have_changed === TRUE) {
      $entity->set($field_name, $values);
      $entity_have_changed = TRUE;
    }

  }

  if ($entity_have_changed === TRUE) {
    $entity->save();
  }
}

/**
 * Update Drupal-media elements attributes with adding context.
 *
 * @param string $value
 *   Text which need an update.
 * @param Drupal\Core\Entity\EntityInterface $entity
 *   Host entity.
 * @param string $field_name
 *   Field name.
 * @param int $delta
 *   Delta.
 *
 * @return string
 *   New text.
 */
function media_contextual_crop_embed_update_entity_process_value($value, $entity, $field_name, $delta) {

  // Find all media_embed items in text.
  $matches = [];
  preg_match_all('|<drupal-media ([^>]*)>|m', $value, $matches);
  foreach ($matches[1] as $item) {
    // If a media embed use CROP and don't have context.
    if (strstr($item, 'data-crop-type') && !strstr($item, 'data-crop-context')) {
      // Create a context.
      $context_base = \Drupal::service('media_contextual_crop.service')->getBaseContext($entity, $field_name, $delta);
      $hash = md5($item);
      // Add context to media embed element.
      $new_crop_info = $item . ' data-crop-context="' . $context_base . ':' . $hash . '"';
      // Replace old media embed attributes to the new with context.
      $value = str_replace($item, $new_crop_info, $value);
    }
  }
  return $value;
}
