/**
 * @file registers the MCCE toolbar button and binds functionality to it.
 */

import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import icon from '../../../../icons/crop.svg';
import {getClosestSelectedDrupalMediaElement} from "./utils";

export default class MCCEUI extends Plugin {
  init() {
    const editor = this.editor;

    const options = [];

    // Add item in media toolbar !
    var toolbar = editor.config.get('drupalMedia.toolbar');
    toolbar.push('|');
    toolbar.push('MCCE');
    editor.config.set('drupalMedia.toolbar', toolbar);

    // This will register the MCCE toolbar button.
    editor.ui.componentFactory.add('MCCE', (locale) => {
      const command = editor.commands.get('insertMCCE');
      const buttonView = new ButtonView(locale);

      // Create the toolbar button.
      buttonView.set({
        label: editor.t('Contextual Crop'),
        icon,
        tooltip: true,
      });

      // Get CKeditor Instance Data
      let dialogSettings = {};


      dialogSettings.defaultViewMode = editor.config['_config'].MCCE['default_media_view_mode'];
      dialogSettings.preview_image_style = editor.config['_config'].MCCE['preview_image_style'];
      dialogSettings.editorInstanceId = jQuery(this.editor.sourceElement).attr('data-ckeditor5-id');
      this.listenTo(buttonView, 'execute', () => {

        const { model } = this.editor;
        const drupalMediaElement = getClosestSelectedDrupalMediaElement(
          model.document.selection,
        );

        let mediaUUID = drupalMediaElement['_attrs'].get('drupalMediaEntityUuid');


        if(drupalMediaElement['_attrs'].get('drupalMediaCropSettings') !== undefined) {
          dialogSettings.oldCrop = drupalMediaElement['_attrs'].get('drupalMediaCropSettings');
        }
        if(drupalMediaElement['_attrs'].get('drupalMediaCropType') !== undefined) {
          dialogSettings.oldCropType = drupalMediaElement['_attrs'].get('drupalMediaCropType');
        }

        // Get selected view mode if not default
        dialogSettings.selectedViewMode = drupalMediaElement['_attrs'].get('drupalElementStyleViewMode') ?? null;

        Drupal.ckeditor5.openDialog(
          '/media-contextual-crop-embed/crop-modal/' + mediaUUID,
          ( attributes ) => {
            editor.execute('insertMCCE', attributes);
          },
          dialogSettings,
        );
      });

      return buttonView;
    });
  }
}
