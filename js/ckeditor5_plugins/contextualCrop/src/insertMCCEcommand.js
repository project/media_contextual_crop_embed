/**
 * @file defines InsertMCCECommand, which is executed when the MCCE
 * toolbar button is pressed.
 */
// cSpell:ignore MCCEediting

import { Command } from 'ckeditor5/src/core';
import { getClosestSelectedDrupalMediaElement } from './utils';
import { METADATA_ERROR } from './utils';
import { md5 } from 'js-md5';
export default class InsertMCCECommand extends Command {
  execute(data) {

    const { model } = this.editor;
    const drupalMediaElement = getClosestSelectedDrupalMediaElement(
      model.document.selection,
    );

    if(data.media_uuid !== drupalMediaElement['_attrs'].get('drupalMediaEntityUuid')) {
      console.log('bad media');
      return;
    }

    let crop_data = {
      'drupalMediaCropType': data.crop['data-crop-type'],
      'drupalMediaCropSettings': data.crop['data-crop-settings'],
      'drupalMediaCropContext': data.crop['data-crop-context']
    }

    if(crop_data.drupalMediaCropContext === undefined) {
      let editor_context = this.editor.sourceElement.attributes.context.value;
      crop_data.drupalMediaCropContext = editor_context + ':' + md5(data.media_uuid + ':' + data.crop['data-crop-settings']);
    }

    model.change((writer) => {
      writer.setAttributes(
        crop_data,
        drupalMediaElement,
      );
    });

  }

  refresh() {
    const drupalMediaElement = getClosestSelectedDrupalMediaElement(
      this.editor.model.document.selection,
    );
    this.isEnabled = !!drupalMediaElement;

    if (this.isEnabled) {
      this.value = drupalMediaElement.getAttribute('drupalMediaCropType');
    } else {
      this.value = false;
    }
  }
}
